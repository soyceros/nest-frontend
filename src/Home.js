import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div>
      <h1>Bienvenidos</h1>
      <Link to="/employees">Ir a la lista de empleados</Link>
    </div>
  );
};

export default Home;
