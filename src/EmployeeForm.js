import React, { useState } from 'react';

const EmployeeForm = ({ employee, onUpdate }) => {
  const [name, setName] = useState(employee.name);
  const [position, setPosition] = useState(employee.position);
  const [salary, setSalary] = useState(employee.salary);

  const handleSubmit = (e) => {
    e.preventDefault();
    onUpdate({
      ...employee,
      name,
      position,
      salary: parseFloat(salary),
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <h3>Update Employee</h3>
      <div>
        <label>Name</label>
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </div>
      <div>
        <label>Position</label>
        <input type="text" value={position} onChange={(e) => setPosition(e.target.value)} />
      </div>
      <div>
        <label>Salary</label>
        <input type="number" value={salary} onChange={(e) => setSalary(parseFloat(e.target.value))} />
      </div>
      <button type="submit">Update</button>
    </form>
  );
};

export default EmployeeForm;
