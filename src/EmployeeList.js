import React, { useState, useEffect } from 'react';
import axios from 'axios';
import EmployeeForm from './EmployeeForm';

const EmployeeList = () => {
  const [employees, setEmployees] = useState([]);
  const [selectedEmployee, setSelectedEmployee] = useState(null);

  useEffect(() => {
    const fetchEmployees = async () => {
      try {
        const response = await axios.get('http://localhost:3000/employees');
        setEmployees(response.data);
      } catch (error) {
        console.error('Failed to fetch employees', error);
      }
    };

    fetchEmployees();
  }, []);

  const handleSelectEmployee = (employee) => {
    setSelectedEmployee(employee);
  };

  const handleUpdate = async (updatedEmployee) => {
    try {
      const response = await axios.patch(`http://localhost:3000/employees/${updatedEmployee.id}`, updatedEmployee);
      setEmployees(employees.map(emp => emp.id === updatedEmployee.id ? response.data : emp));
      setSelectedEmployee(null); // Clear selection after update
    } catch (error) {
      console.error('Failed to update employee', error);
    }
  };

  return (
    <div>
      <h2>Employee List</h2>
      <ul>
        {employees.map(employee => (
          <li key={employee.id}>
            {employee.name} - {employee.position}
            <button onClick={() => handleSelectEmployee(employee)} style={{ marginLeft: '10px' }}>Edit</button>
          </li>
        ))}
      </ul>
      {selectedEmployee && (
        <EmployeeForm key={selectedEmployee.id} employee={selectedEmployee} onUpdate={handleUpdate} />
      )}
    </div>
  );
};

export default EmployeeList;
